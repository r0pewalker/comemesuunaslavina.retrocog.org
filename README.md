COME ME SU UNA SLAVINA

A collectively sourced tribute blog.

Timeline:

* 2014: la Slavina is born on Tumblr
  comemesuunaslavina.tumblr.com;
* 2018: Tumblr dies a horrible death at the hands of Yahoo's content policy;
* 2020: the creators assemble again to restore the Slavina as a Ghost blog
  comemesuunaslavina.retrocog.org;    
* 2021: la Slavina has become a monument, devoid of life.
  The creators set it in stone for eons to come.
